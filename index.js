import https from 'https';
import fs from 'fs';
import app from './app.js';

const PORT = process.env.PORT || 3000;
const HTTPS_PORT = 443; // Port HTTPS

// Read file keys and SSL certificates
const options = {
    key: fs.readFileSync('./key.pem'),
    cert: fs.readFileSync('./cert.pem')
};

// Create an HTTPS server
const server = https.createServer(options, app);

// Listen for connections on the HTTPS port
server.listen(HTTPS_PORT, () => {
    console.log(`Server is running on port ${HTTPS_PORT}`);
});
