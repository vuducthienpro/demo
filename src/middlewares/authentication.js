import passport from 'passport';
import { Strategy as JwtStrategy, ExtractJwt } from 'passport-jwt';
import jwt from 'jsonwebtoken';
import ApiError from '../utils/apiError.js';

const jwtOptions = {
    secretOrKey: process.env.JWT_SECRET,
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
};

passport.use(
    new JwtStrategy(jwtOptions, (payload, done) => {
        if (payload.userId) {
            return done(null, payload);
        } else {
            return done(new Error('User not found'), false);
        }
    })
);

const authentication = (req, res, next) => {
    passport.authenticate('jwt', { session: false }, (err, user) => {
        if (err) {
            throw new ApiError(401, 'Token is not valid');
        }
        if (!user) {
            throw new ApiError(401, 'No token provided, authorization denied');
        }
        req.user = user;
        next();
    })(req, res, next);
};

export default authentication;
