import ApiError from '../utils/apiError.js';

const authorization = (roles) => {
    return (req, res, next) => {
        if (!roles.includes(req.user.role)) {
            throw new ApiError(403, 'Access denied');
        }
        next();
    };
};

export default authorization;
