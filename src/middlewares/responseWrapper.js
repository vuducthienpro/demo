const responseWrapperMiddleware = (req, res, next) => {
  const originalJson = res.json.bind(res);

  res.json = (body) => {
    const responseBody = {
      status: res.statusCode,
      data: (res.statusCode >= 200 && res.statusCode < 300) ? body : undefined,
      error: (res.statusCode < 200 || res.statusCode >= 300) ? body : undefined
    };

    originalJson(responseBody);
  };

  next();
}

export default responseWrapperMiddleware;
