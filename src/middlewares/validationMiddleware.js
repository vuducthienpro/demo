const validateInput = (req, res, next) => {
    const { email, password, name } = req.body;
    if (!email || !password || !name) {
        return res.status(400).json({ error: 'Missing email, password, or name' });
    }
    next();
};

const validateData = (req, res, next) => {
    const { email, password } = req.body;
    if (!email || !password) {
        return res.status(400).json({ error: 'Missing email, password' });
    }
    next();
};

export default { validateInput, validateData };
