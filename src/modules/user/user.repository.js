import User from './user.model.js';
// import { exceptionHandlerMiddleware } from "../../middlewares/index.js"

const getUsers = async () => {
    return await User.findAll({ attributes: ['id', 'email'] });
};

const createUser = async (name, email, password) => {
    return await User.create({ name, email, password });
};

const getUserByEmail = async (email) => {
    return await User.findOne({ where: { email } });
};


export default { getUsers: getUsers, createUser: createUser, getUserByEmail: getUserByEmail }