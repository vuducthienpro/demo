import userService from './user.service.js';

const getUsers = async (req, res, next) => {
    try {
        const users = await userService.getUsers();
        res.status(200).json(users);
    } catch (err) {
        next(err);
    }
};

const createUser = async (req, res, next) => {
    try {
        const { name, email, password } = req.body;
        await userService.createUser(name, email, password);
        res.status(201).json({ message: 'Create successfuly' });
    } catch (err) {
        next(err);
    }
};


const authenticateUser = async (req, res, next) => {
    try {
        const { email, password } = req.body;
        const token = await userService.authenticateUser(email, password);
        res.status(200).json({ token });
    } catch (err) {
        next(err);
    }
};

export default { getUsers, createUser, authenticateUser }