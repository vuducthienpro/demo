import userRepository from './user.repository.js';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';

const getUsers = async () => {
    return await userRepository.getUsers();
};

const createUser = async (name, email, password) => {
    const hashedPassword = await bcrypt.hash(password, 10);
    return await userRepository.createUser(name, email, hashedPassword);
};

const authenticateUser = async (email, password) => {
    const user = await userRepository.getUserByEmail(email);
    if (!user) {
        throw new Error('Invalid credentials');
    }
    const isPasswordValid = await bcrypt.compare(password, user.password);
    if (!isPasswordValid) {
        throw new Error('Invalid credentials');
    }
    const token = jwt.sign({ userId: user.id }, process.env.JWT_SECRET);
    return token;
};

export default { getUsers, createUser, authenticateUser }
