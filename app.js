import express from 'express';
import userRoutes from './src/modules/user/index.js';
import { authenticationMiddleware, responseWrapperMiddleware, exceptionHandlerMiddleware, validationMiddleware } from './src/middlewares/index.js';
import helmet from 'helmet';
import rateLimit from 'express-rate-limit';
import session from 'express-session';
import crypto from 'crypto';

const app = express();
app.use(express.json());

// Helmet middleware
app.use(helmet());

// Rate limiting middleware
const limiter = rateLimit({
    windowMs: 15 * 60 * 1000, // 15 minutes
    max: 100,
});
app.use(limiter);

// Session middleware
const secret = crypto.randomBytes(32).toString('hex');
app.use(session({
    secret: secret,
    resave: false,
    saveUninitialized: true,
    cookie: { secure: true } // Secure cookie
}));

// Use middleware to wrap the response before using the router
app.use(responseWrapperMiddleware);

const apiRouter = express.Router();

// Add routes to the router
apiRouter.get('/users', authenticationMiddleware, userRoutes.getUsers);
apiRouter.post('/users', validationMiddleware.validateInput, userRoutes.createUser);
apiRouter.post('/users/authenticate', validationMiddleware.validateData, userRoutes.authenticateUser);

// Use routers
app.use('/api', apiRouter);

// Use exception handling middleware at the end to catch and handle exceptions
app.use(exceptionHandlerMiddleware);

export default app;
